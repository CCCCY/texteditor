import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;




public class ClientCommunicator extends Thread{
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private MainWindowGUI mainwindow;
	
	public ClientCommunicator(Socket inSocket,MainWindowGUI mainwindow) {
		s = inSocket;
		this.mainwindow = mainwindow;
		boolean socketReady = initializeVariables();
		if (socketReady) {
			start();
		}
	}

	private boolean initializeVariables() {
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());

		} catch (IOException ioe) {
			return false;
		}
		return true;
	}
	

	public void run() {
		try {
			while(true) {
				// in case the server sends another factory to us
				UserProfile newuser = (UserProfile)ois.readObject();
				if(newuser.getCommand().equals(newuser.command_loginverify)){
					if(newuser.loginapproved){
						mainwindow.enterlogin(newuser,this);
					}
					else{
						invalidpopup("Log-in");
					}
				}
				if(newuser.getCommand().equals(newuser.command_signupverify)){
					if(newuser.signupapproved){
						mainwindow.entersignup(newuser,this);
					}
					else{
						invalidpopup("Sign-Up");
					}
				}
				if(newuser.getCommand().equals(newuser.command_uploadverify)){
					mainwindow.onlinesavefeedback(newuser);

				}
				if(newuser.getCommand().equals(newuser.command_downloadverify)){
					mainwindow.onlineopenfeedback(newuser);
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	public void senduploadattempt(UserProfile uploaduser){
		uploaduser.setCommand(uploaduser.command_uploadverify);
		try {
			oos.reset();
			oos.writeObject(uploaduser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void senddownloadattempt(UserProfile downloaduser){
		downloaduser.setCommand(downloaduser.command_downloadverify);
		try {
			oos.reset();
			oos.writeObject(downloaduser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void sendsignupattempt(UserProfile attemptuser){
		attemptuser.setCommand(attemptuser.command_signupattempt);
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sendloginattempt(UserProfile attemptuser){
		attemptuser.setCommand(attemptuser.command_loginattempt);
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void invalidpopup(String message){
		JOptionPane.showMessageDialog(mainwindow, "Username or password is invalid", message +"Failed",
				JOptionPane.ERROR_MESSAGE);
	}

}
