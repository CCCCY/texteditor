import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerCommunicator extends Thread{
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private OfficeServerGUI servergui;
	private OfficeServerListener osl;
	private OfficeSQLDriver officesql;

	public ServerCommunicator(OfficeServerListener osl, Socket s,OfficeServerGUI parentgui,OfficeSQLDriver officesql){
		servergui = parentgui;
		this.officesql = officesql;
		this.osl = osl;
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		
	}

	public void run() {
		try {
			while(true) {
				UserProfile attemptuser = null;
				attemptuser = (UserProfile)ois.readObject();
				if(attemptuser.getCommand().equals(attemptuser.command_loginattempt)){
					servergui.setLoginAttempt(attemptuser);
					verifylogin(attemptuser);

				}
				if(attemptuser.getCommand().equals(attemptuser.command_signupattempt)){
					servergui.setSignupAttempt(attemptuser);
					verifysignup(attemptuser);
				}
				if(attemptuser.getCommand().equals(attemptuser.command_uploadverify)){
					verifyupload(attemptuser);
					servergui.setUploadAttempt(attemptuser);
				}
				if(attemptuser.getCommand().equals(attemptuser.command_downloadverify)){
					verifydownload(attemptuser);
					servergui.setDownloadAttempt(attemptuser);
				}
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe in run: " + ioe.getMessage());
			osl.removeCommunicator(this);
			
		}
	}
	private void verifydownload(UserProfile attemptuser){
		boolean success = officesql.downloadFile(attemptuser);
		attemptuser.downloadsuccess = success;
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
	}
	private void verifyupload(UserProfile attemptuser){
		boolean success = officesql.uploadFile(attemptuser);
		attemptuser.uploadsuccess = success;
		
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
	}
	public void verifylogin(UserProfile loginuser){
		
		//verify if login is valid
		boolean success = verifyLoginDataBase(loginuser);
		loginuser.loginapproved = success;
		loginuser.setCommand(loginuser.command_loginverify);
		servergui.setLoginVerify(loginuser.getName(),success,loginuser.getEncryptedPassword());
			
		try {
			oos.reset();
			oos.writeObject(loginuser);
			oos.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
	}
	public void verifysignup(UserProfile loginuser){
		
		boolean success = verifySignupDataBase(loginuser);
		servergui.setSignupVerify(loginuser.getName(),success);

		loginuser.signupapproved = success;
		loginuser.setCommand(loginuser.command_signupverify);
		try {
			oos.reset();
			oos.writeObject(loginuser);
			oos.flush();
		} catch (IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
			

		
		
	}
	private boolean verifyLoginDataBase(UserProfile loginuser){
		if(officesql.exist(loginuser.getName())){
			if(officesql.verifylogin(loginuser)){
				return true;
			}
		}
		return false;
	}
	private boolean verifySignupDataBase(UserProfile signupuser){
		if(!officesql.exist(signupuser.getName())){

			System.out.println("doesnot exist");
			officesql.addUserProfile(signupuser);
			return true;
		}
		return false;
	}
	
}
