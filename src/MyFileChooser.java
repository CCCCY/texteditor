import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class MyFileChooser extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserProfile user;
	private JButton opensavebutton; 
	private JButton cancelbutton;
	private JTextField filefield;
	private JPanel middlepanel;
	private String command;
	private JTextArea ta;
	private MainWindowGUI maingui;
	private Vector<JLabel> alllabel;
	private final static String filepath = "Resources/files/";
	public MyFileChooser(Frame frame, UserProfile currentuser,String command,JTextArea ta){
		
		super(frame,"File Chooser");
		maingui = (MainWindowGUI) frame;
		alllabel = new Vector<JLabel>();
		this.ta = ta;
		this.command = command;
		user = currentuser;
		this.setSize(500, 200);
		setLocation(100,100);
		createVariables();
		createGUI();
		setVisible(true);
	}
	public MyFileChooser(Frame frame, UserProfile currentuser,String command){
		
		super(frame,"File Chooser");
		maingui = (MainWindowGUI) frame;
		this.command = command;
		alllabel = new Vector<JLabel>();
		user = currentuser;
		this.setSize(500, 200);
		setLocation(100,100);
		createVariables();
		createGUI();
		setVisible(true);
	}
	private void createVariables(){
		if(command.equals("open")){
			opensavebutton = new JButton("Open");
			opensavebutton.addActionListener(new OpenListener());
		}
		if(command.equals("save")){
			opensavebutton = new JButton("Save");
			opensavebutton.addActionListener(new SaveListener());
			
		}
		cancelbutton = new JButton("Cancel");
		cancelbutton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				MyFileChooser.this.setVisible(false);
			}
			
		});
		filefield = new JTextField(20);
	//	filefield.setEditable(false);
		filefield.setBackground(Color.white);
		middlepanel = new JPanel();
		
	}
	private void createGUI(){
		this.setLayout(new BorderLayout());
		JPanel toppanel = new JPanel();
		toppanel.setLayout(new BoxLayout(toppanel,BoxLayout.X_AXIS));
		toppanel.add(new JLabel("Select a file:"));
		toppanel.add(Box.createGlue());
		middlepanel.setBackground(Color.white);
		middlepanel.setLayout(new GridLayout(4,5));
	
		JScrollPane js = new JScrollPane(middlepanel);

		JPanel buttompanel = new JPanel();
		buttompanel.setLayout(new GridLayout(2,1));
		JPanel grid1 = new JPanel();
		grid1.setLayout(new BoxLayout(grid1,BoxLayout.X_AXIS));
		grid1.add(new JLabel("File"));
		grid1.add(filefield);
		grid1.add(Box.createGlue());

		JPanel grid2 = new JPanel();
		grid2.setLayout(new BoxLayout(grid2,BoxLayout.X_AXIS));
		grid2.add(Box.createGlue());
		grid2.add(cancelbutton);
		grid2.add(opensavebutton);
		buttompanel.add(grid1);
		buttompanel.add(grid2);
		add(toppanel, BorderLayout.NORTH);
		add(js, BorderLayout.CENTER);
		add(buttompanel, BorderLayout.SOUTH);

		
		//add files
		
		showfiles();
	}
	private void showfiles(){
		int j=20;
		if(user.files.size()==0)return;
		if(user.files!=null){
			for(int i=0; i<user.files.size();i++){
				String file = user.files.get(i);
				addFile(file);
				j--;
			}
		}
		for(int i=0;i<j;i++){
			JLabel newfile = new JLabel("");
			Border paddingBorder = BorderFactory.createEmptyBorder(0,0,0,10);
			newfile.setBorder(paddingBorder);
			newfile.setOpaque(true);
			newfile.setBackground(Color.white);
			middlepanel.add(newfile);
			alllabel.add(newfile);

		}
		
		
	}
	private void addFile(String filename){
		JLabel newfile = new JLabel(filename);
		Border paddingBorder = BorderFactory.createEmptyBorder(0,0,0,10);
		newfile.setBorder(paddingBorder);

		newfile.setOpaque(true);
		newfile.setBackground(Color.white);
		newfile.addMouseListener(new filelabelListener(newfile));
		alllabel.add(newfile);
		middlepanel.add(newfile);

	}
	private boolean existfile(){
		for(JLabel label:alllabel){
			if(label.getText().equals(filefield.getText())){
				return true;
			}
		}
		return false;
	}
	class SaveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(filefield.getText().length()==0)return;
			
			String filename = filefield.getText(); 
			if(existfile()){
				int response = JOptionPane.showConfirmDialog(maingui,filename+
				        		" already exists. Do you want to replace it?",
				                  "Ovewrite file", JOptionPane.YES_NO_OPTION,
				                  JOptionPane.WARNING_MESSAGE);
				if (response == JOptionPane.NO_OPTION)
				                  return;
			}
	
			String filecontent = ta.getText();
			user.uploadfile(filename, filecontent);
			maingui.com.senduploadattempt(user);
			
		}
		
	}
	
	class OpenListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(filefield.getText().length()==0)return;		
			String filename = filefield.getText(); 
			user.downloadfile(filename);
			maingui.com.senddownloadattempt(user);
		}
		
	}
	class filelabelListener extends MouseAdapter{
		JLabel filelabel;
		public filelabelListener(JLabel filelabel){
			this.filelabel = filelabel;

			
		}
		public void mousePressed(MouseEvent e)
	    {
			for(JLabel label:alllabel){
				label.setBackground(Color.white);
			}

			filelabel.setBackground(Color.blue);
			filefield.setText(filelabel.getText());
	        
	    }
		public void mouseReleased(MouseEvent e)
		{
		//	filelabel.setBackground(Color.white);

	    }
	}
	public void savefeedback(UserProfile userback){
		this.user = userback;
		this.setVisible(false);
		if(user.uploadsuccess){
	        maingui.mytabpane.setTitleAt(maingui.mytabpane.getSelectedIndex(), user.getFileName());
			JOptionPane.showMessageDialog(maingui,
				    "File successfully saved");
			user.uploadsuccess = false;
		}else{
			JOptionPane.showMessageDialog(maingui, "File failed to save","Message",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	public void openfeedback(UserProfile userback){
		this.user = userback;
		System.out.println("open success");
		this.setVisible(false);
		if(user.downloadsuccess){
			maingui.onlineopennew();
			user.downloadsuccess = false;
		}else{
			JOptionPane.showMessageDialog(maingui, "File failed to download","Message",
					JOptionPane.ERROR_MESSAGE);
		}
		

	}

}
