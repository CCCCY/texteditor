import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class MySignUpPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Image BackgroundImage;
	private MyButton LoginButton;
	private MainWindowGUI mainwindow;
	private JLabel usernamelabel;
	private JLabel passwordlabel;
	private JTextField usernamefield;
	private JPasswordField passwordfield;
	private JLabel repeatlabel;
	private JPasswordField repeatfield;
	//network variables
	private String hostname;
	private int port;
	public MySignUpPanel(MainWindowGUI windowin, String hostname, int port){
		super();
		this.hostname = hostname;
		this.port = port;
		setFont();
		mainwindow = windowin;
		this.setBackground(Color.gray);
		try {
			BackgroundImage =ImageIO.read(new File("Resources/img/backgrounds/trojan.png"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initializeVariables();
		createGUI();
		
	}
	private void initializeVariables(){
		LoginButton = new MyButton("Login");
		LoginButton.addActionListener(new SignupListener());
		usernamelabel = new JLabel("username: ");
		passwordlabel = new JLabel("password: ");
		repeatlabel = new JLabel("repeat: ");
		usernamefield = new JTextField(18);
		passwordfield = new JPasswordField(18);
		repeatfield = new JPasswordField(18);
		
	}
	private void createGUI(){
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(usernamelabel,gbc);
		gbc.gridx = 1;
		add(usernamefield,gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(passwordlabel,gbc);
		gbc.gridx = 1;
		add(passwordfield,gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(repeatlabel,gbc);
		gbc.gridx = 1;
		add(repeatfield,gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		add(LoginButton, gbc);
		
	}
	private void setFont(){
		try {
			//http://stackoverflow.com/questions/5652344/how-can-i-use-a-custom-font-in-java
		    Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Resources/fonts/kenvector_future.ttf")).deriveFont(13f);
			UIManager.put("Button.font",customFont);
			UIManager.put("Label.font",customFont);

		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}  
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = (this.getWidth()-BackgroundImage.getWidth(null))/2;
		int height =  (this.getHeight()-BackgroundImage.getWidth(null))/2+30;
		g.drawImage(BackgroundImage, width, height-5,null);
	}
	
	private class SignupListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub		
			//if repeat and password does not match
			if(usernamefield.getText().length()==0 ||passwordfield.getText().length()==0 ||repeatfield.getText().length()==0)return;
			if(!repeatfield.getText().equals(passwordfield.getText())){
				JOptionPane.showMessageDialog(MySignUpPanel.this,
					    "Password and the repeat do not match",
					    "Sign-up Failed",
					    JOptionPane.WARNING_MESSAGE);
				return;
			}
			boolean hasUppercase = !passwordfield.getText().equals(passwordfield.getText().toLowerCase());
			boolean hasNumber = !passwordfield.getText().equals(passwordfield.getText().replaceAll("[*0-9]", ""));
			if(!hasUppercase||!hasNumber){
				JOptionPane.showMessageDialog(MySignUpPanel.this,
					    "The password must contain at least:\n 1-number 1-Uppercase letter ",
					    "Sign-up Failed",
					    JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			
			
			
			
			//if sign up information is okay
			Socket s = null;
			try {
				s = new Socket(hostname, port);
				UserProfile attemptuser = new UserProfile(usernamefield.getText(),passwordfield.getText());
				ClientCommunicator com = new ClientCommunicator(s, mainwindow);
				com.sendsignupattempt(attemptuser);
					
			} catch (IOException ioe) {
				JOptionPane.showMessageDialog(MySignUpPanel.this,
					    "Server cannot be reached.\n Program is in offline mode",
					    "Sign-up Failed",
					    JOptionPane.WARNING_MESSAGE);
				System.out.println("ioe: " + ioe.getMessage());
				mainwindow.enterOffline();
			} 
		}
	}

}
