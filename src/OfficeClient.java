import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.UIManager;

public class OfficeClient {
	public OfficeClient(String hostname,int portnumber){
		
		MainWindowGUI mywindow = new MainWindowGUI(hostname,portnumber);
		mywindow.setVisible(true);
	}

	public static void main(String [] args){
		try{
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());			
		}catch(Exception e){
			System.out.println("Warning! Crossing-platform L&F not used");
		}
		
		try {
			int portnumber = 0;
			String hostname=null;
			Scanner text = new Scanner(new File("clientconfigure.txt"));
			if(text.hasNext())hostname = text.next();
			if(text.hasNext())portnumber = text.nextInt();
			new OfficeClient(hostname,portnumber);
			text.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
}
