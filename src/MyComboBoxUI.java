


import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

//http://stackoverflow.com/questions/3008447/how-can-i-change-the-arrow-style-in-a-jcombobox
public class MyComboBoxUI extends BasicComboBoxUI{
	
	@Override 
	protected JButton createArrowButton() {
		JButton DropdownButton =null;
		
            DropdownButton = new JButton(){
    			private static final long serialVersionUID = 1L;
    			protected void paintComponent(Graphics g){
    				Image dropdownImage = null;
    				try {
    		            dropdownImage = ImageIO.read(new File("Resources/img/menu/red_sliderDown.png"));
    		            g.drawImage(dropdownImage, 0, 0,this.getWidth(),this.getHeight(), null);
    				} catch (IOException e){
    		        	e.printStackTrace();
    		        }
    				
        			
        		}
        	};
         //   imageTrack = ImageIO.read(new File("Resources/img/scrollbar/red_sliderDown.png"));
        
        return DropdownButton;
    }
}
