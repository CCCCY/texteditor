
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class MyScrollBar extends BasicScrollBarUI{
	//http://stackoverflow.com/questions/12265740/make-a-custom-jscrollbar-using-an-image
	
	private Image imageThumb, increaseImage, decreaseImage, trackImage;
	private JButton slideupButton, slidedownButton;
	MyScrollBar() {
        try {
            imageThumb = ImageIO.read(new File("Resources/img/scrollbar/red_button05.png"));
            increaseImage = ImageIO.read(new File("Resources/img/scrollbar/red_sliderUp.png"));
            decreaseImage = ImageIO.read(new File("Resources/img/scrollbar/red_sliderDown.png"));
            trackImage = ImageIO.read(new File("Resources/img/scrollbar/red_button03.png"));

            createButtons();
        } catch (IOException e){
        	e.printStackTrace();
        }
    }

    
	//http://stackoverflow.com/questions/12265740/make-a-custom-jscrollbar-using-an-image
    private void createButtons(){
    	slideupButton = new JButton(){
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g){
				g.drawImage(increaseImage, 0, 0,this.getWidth(),this.getHeight(), null);
    			
    		}
    	};
    	slidedownButton = new JButton(){
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g){
				g.drawImage(decreaseImage, 0, 0,this.getWidth(),this.getHeight(), null);

    		}
    	};
    }
    //http://stackoverflow.com/questions/12265740/make-a-custom-jscrollbar-using-an-image

    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {        
        g.translate(thumbBounds.x, thumbBounds.y);
      //  g.setColor( Color.red );
        g.drawRect( 0, 0, thumbBounds.width - 2, thumbBounds.height - 1 );
        AffineTransform transform = AffineTransform.getScaleInstance((double)thumbBounds.width/imageThumb.getWidth(null),(double)thumbBounds.height/imageThumb.getHeight(null));
        ((Graphics2D)g).drawImage(imageThumb, transform, null);
        g.translate( -thumbBounds.x, -thumbBounds.y );
    }
    //http://stackoverflow.com/questions/12265740/make-a-custom-jscrollbar-using-an-image

    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {        
    	g.translate(trackBounds.x, trackBounds.y);
        ((Graphics2D)g).drawImage(trackImage,AffineTransform.getScaleInstance((double)trackBounds.width/trackImage.getWidth(null),(double)trackBounds.height/trackImage.getHeight(null)),null);
        g.translate( -trackBounds.x, -trackBounds.y );
    }

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return slideupButton;
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return slidedownButton;
    }

  
}
