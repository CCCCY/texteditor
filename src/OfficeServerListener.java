import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;



public class OfficeServerListener extends Thread  {
	private ServerSocket ss;
	private int portnumber;
	private OfficeServerGUI servergui;
	private Vector<ServerCommunicator> communicators;
	private OfficeSQLDriver officesql;
	public OfficeServerListener(int port, OfficeServerGUI parentgui,OfficeSQLDriver officesql){
		this.officesql = officesql;
		portnumber = port;
		servergui = parentgui;
		communicators = new Vector<ServerCommunicator>();
		ss = null;
		try {
			ss = new ServerSocket(portnumber);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void run() {
		try {
			while (true){			
				Socket s = ss.accept();
				ServerCommunicator communicator = new ServerCommunicator(this,s,servergui,officesql);
				communicators.add(communicator);
			}
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe) {
					System.out.println("ioe closing ss: " + ioe.getMessage());
				}
			}
		}
	}
	public void stopconnection(){
		if (ss != null) {
			try {
				ss.close();
			} catch (IOException ioe) {
				System.out.println("ioe closing ss: " + ioe.getMessage());
			}
		}
		
	}
	public void removeCommunicator(ServerCommunicator sc){
		communicators.remove(sc);
	}
}
