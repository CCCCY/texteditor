
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.ColorUIResource;
import javax.swing.text.Highlighter;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;



public class MainWindowGUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	myMenuBar mybar;
	public JTabbedPane mytabpane;
	private Vector<MyTextArea> mytexts;
	private Vector<File> openedfiles;
	MyInitialPanel InitialPanel;
	MyBackgroundPanel BackgroundPanel;
	MyLoginPanel LoginPanel;
	MySignUpPanel SignupPanel;
	private Image IconImage;
	private Font customFont;
	private boolean isOffline;
	private String hostname;
	private int port;
	private UserProfile user;
	public ClientCommunicator com;
	private MyFileChooser onlinefilechooser;
	public MainWindowGUI(String hostname, int port){
		super("201 Office");
		this.hostname = hostname;
		this.port = port;
		setSize(700,400);
		setLocation(300,100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setFont();
		createComponents();
		createGUI();
		createActions();
		setImages();
		getContentPane().setBackground(Color.gray); 
		setInitialPanel();
		//Whatever color
		setVisible(true);


	}
	private void createComponents(){
		isOffline = true;
		mybar = new myMenuBar();
		mytabpane = new JTabbedPane();
		mytexts = new Vector<MyTextArea>();
		openedfiles = new Vector<File>();
		BackgroundPanel = new MyBackgroundPanel();
		LoginPanel = new MyLoginPanel(this,hostname,port);
		SignupPanel = new MySignUpPanel(this,hostname,port);

	}
	private void createGUI(){
		//http://java-demos.blogspot.com/2013/10/how-to-set-custom-image-cursor-in-java.html
		 try{
	        setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("Resources/img/icon/cursor.png").getImage(),new Point(0,0),"custom cursor"));
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 System.out.println("as");
	     }
	}
	
	public void enterOffline(){
		InitialPanel.setVisible(false);
		LoginPanel.setVisible(false);
		SignupPanel.setVisible(false);
		isOffline = true;
		setJMenuBar(mybar);
		add(BackgroundPanel);
	}
	public void enterlogin(UserProfile user,ClientCommunicator com){
		this.user = user;
		this.com = com;
		LoginPanel.setVisible(false);
		isOffline = false;
		setJMenuBar(mybar);
		add(BackgroundPanel);
	}
	public void entersignup(UserProfile user,ClientCommunicator com){
		this.user = user;
		this.com = com;
		SignupPanel.setVisible(false);
		isOffline = false;
		setJMenuBar(mybar);
		add(BackgroundPanel);
	}
	
	
	private void createActions(){
		mybar.newitem.addActionListener(new NewActionListener());
		mybar.openitem.addActionListener(new OpenActionListener(this));
		mybar.saveitem.addActionListener(new SaveActionListener(this));
		mybar.closeitem.addActionListener(new CloseActionListener());
		mybar.runitem.addActionListener(new RunActionListener());
		mybar.configureitem.addActionListener(new ConfigureActionListener());
	

	}
	
	private void implementTextArea(MyTextArea ta){
		UndoManager manager = new UndoManager();
		Action undoAction = new UndoAction(manager);
		Action redoAction = new RedoAction(manager);
		mybar.undoitem.addActionListener(undoAction);
		mybar.redoitem.addActionListener(redoAction);
		ta.getTA().getDocument().addUndoableEditListener(manager);
		mybar.undoitem.setEnabled(manager.canUndo());
		mybar.redoitem.setEnabled(manager.canRedo());
		ta.getTA().getDocument().addDocumentListener(new DocumentListener() {
	        @Override
	        public void removeUpdate(DocumentEvent e) {
	        	mybar.undoitem.setEnabled(true);
	    		mybar.redoitem.setEnabled(true);
	        }
	        @Override
	        public void insertUpdate(DocumentEvent e) {
	        	mybar.undoitem.setEnabled(true);
	    		mybar.redoitem.setEnabled(true);
	        }
	        @Override
	        public void changedUpdate(DocumentEvent arg0) {
	        	mybar.undoitem.setEnabled(manager.canUndo());
	    		mybar.redoitem.setEnabled(manager.canRedo());
	        }
			
	    });
		mybar.copyitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.getTA().copy();
			}
		});
		mybar.cutitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.getTA().cut();
			}
		});
		mybar.pasteitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.getTA().paste();
			}
		});
		mybar.selectitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.getTA().selectAll();
			}
		});
		
	}
	private void checkbar(){
		if(mytexts.isEmpty()){
			mybar.addtwo();
		}
	}
	private void removebar(){
		if(mytexts.isEmpty()){
			mybar.removetwo();
		}
	}
	private void checkremovelogo(){
		if(mytexts.size()==0){
			remove(mytabpane);
			//add(BackgroundPanel);
			BackgroundPanel = new MyBackgroundPanel();
			this.add(BackgroundPanel);
		}
	}
	private void checkaddlogo(){
		if(mytexts.size()==0){
			remove(BackgroundPanel);
			add(mytabpane);
		}
	}
	//http://www.coderanch.com/t/342682/GUI/java/JFileChooser-input-textfield
	private  void presetFilename(Component[] comp,File file){
		System.out.println(file.getName());
	    for(int x = 0; x < comp.length; x++)
	    {
	      if(comp[x] instanceof JPanel) presetFilename(((JPanel)comp[x]).getComponents(),file);
	      else if(comp[x] instanceof JTextField)
	      {
	        ((JTextField)comp[x]).setText(file.getName());;
	        return;
	      }
	    }
	  }
	//private classes	
	private class NewActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			checkaddlogo();
			MyTextArea newtext = new MyTextArea();
			checkbar();
			mytexts.add(newtext);
			mytabpane.addTab("New", newtext.getSP());
			mytabpane.setSelectedIndex(mytexts.size()-1);
			implementTextArea(newtext);
			checkaddlogo();
			
		}
	}
	private class OpenActionListener implements ActionListener{
		MainWindowGUI mainwindow;
		public OpenActionListener(MainWindowGUI windowin){
			mainwindow = windowin;
		}
		public void actionPerformed(ActionEvent e){
			if(isOffline) offlineopen();
			else{
		//https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html
				String[] buttons = { "Online", "Offline" };

			    int rc = JOptionPane.showOptionDialog(mainwindow, "Where would you like to open the file?", "Open...",
			    		JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE, null, buttons, buttons[1]);
			    if(rc == 0) onlineopen();
			    if(rc == 1) offlineopen();
			}
		}
	}
	private void onlineopen(){
		onlinefilechooser = new MyFileChooser(this, user,"open");
	}
	public void onlineopennew(){
		checkaddlogo();
		MyTextArea newtext = new MyTextArea();
		newtext.setText(user.getFileContent());
		checkbar();
		mytexts.add(newtext);
		mytabpane.addTab(user.getFileName(), newtext.getSP());
		mytabpane.setSelectedIndex(mytexts.size()-1);
		implementTextArea(newtext);
		checkaddlogo();
		
	}
	public void onlineopenfeedback(UserProfile userback){
		this.user = userback;
		onlinefilechooser.openfeedback(userback);
	}
	private void offlineopen(){
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open File...");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setMultiSelectionEnabled(false);
        fc.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        Component parent = null;
        int returnVal = fc.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION){
        	File openedfile = fc.getSelectedFile();
        	if(!openedfiles.contains(openedfile)){
        		MyTextArea openedarea = new MyTextArea(openedfile);
    			checkbar();
    			checkaddlogo();
        		mytexts.add(openedarea);
        		mytabpane.addTab(openedfile.getName(), openedarea.getSP());
        		openedfiles.add(openedfile);
    			mytabpane.setSelectedIndex(mytexts.size()-1);
    			implementTextArea(openedarea);

        	}
        	else{
        		JOptionPane.showMessageDialog(parent, "file already opened in editor");
        		return;
        	}
        }
	}
	private class SaveActionListener implements ActionListener{
		MainWindowGUI mainwindow;
		public SaveActionListener(MainWindowGUI windowin){
			mainwindow = windowin;
		}
		public void actionPerformed(ActionEvent e){
			if(mytexts.size() == 0) return;
			if(isOffline) offlinesave();
			else{
		//https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html
				String[] buttons = { "Online", "Offline" };
			    int rc = JOptionPane.showOptionDialog(mainwindow, "Where would you like to save the file?", "SAVE...",
			    		JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE, null, buttons, buttons[1]);
			    if(rc == 0) onlinesave();
			    if(rc == 1) offlinesave();
			}
		}
		
	}
	public void onlinesavefeedback(UserProfile userback){
		this.user = userback;
		onlinefilechooser.savefeedback(userback);
	}
	private void onlinesave(){
		int index = mytabpane.getSelectedIndex();
		JTextArea ta = mytexts.get(index).getTA();
		
		onlinefilechooser = new MyFileChooser(this, user,"save",ta);
		

		
	}
	private void offlinesave(){
		if(mytexts.size() == 0) return;
		int index = mytabpane.getSelectedIndex();
		JTextArea ta = mytexts.get(index).getTA();
		JFileChooser SaveAs = new JFileChooser();
		SaveAs.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
		File file;
		if(openedfiles.contains(mytexts.get(index).getFile())){
			file = mytexts.get(index).getFile();
			System.out.println("exist");
			presetFilename(SaveAs.getComponents(), file);
		}
		SaveAs.setApproveButtonText("Save");
		SaveAs.setDialogTitle("Save As...");
		Component parent = null;
		int actionDialog = SaveAs.showSaveDialog(parent);
		
		if (actionDialog != JFileChooser.APPROVE_OPTION) {
	        return;
	    }			
		file = SaveAs.getSelectedFile();		
        if(file.exists())
        {
        	int response = JOptionPane.showConfirmDialog(parent,SaveAs.getSelectedFile().getName()+
            		" already exists. Do you want to replace it?",
	                  "Ovewrite file", JOptionPane.YES_NO_OPTION,
	                  JOptionPane.WARNING_MESSAGE);
	        if (response == JOptionPane.NO_OPTION)
	                  return;
        }
        
        mytabpane.setTitleAt(index, file.getName());
	    BufferedWriter outFile = null;
	    openedfiles.add(file);
	    mytexts.get(index).setFile(file);
		//http://stackoverflow.com/questions/9690686/save-a-the-text-from-a-jtextarea-ie-save-as-into-a-new-txt-file

	    try {
	    	outFile = new BufferedWriter(new FileWriter(file));
	    	ta.write(outFile);   // *** here: ***

	    } catch (IOException ex) {
	    	ex.printStackTrace();
	    } finally {
	    	if (outFile != null) {
	    		try {
	               outFile.close();
	            } catch (IOException e1) {
	               // one of the few times that I think that it's OK
	               // to leave this blank
	            }
	         }
	      }
		
	}
	public void setUser(UserProfile inuser){
		this.user = inuser;
	}
	
	private class CloseActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(mytexts.isEmpty())return;
			int index = mytabpane.getSelectedIndex();
			if(openedfiles.contains(mytexts.get(index).getFile())){
				openedfiles.remove(mytexts.get(index).getFile());
			}
			mytexts.remove(index);
			mytabpane.remove(index);
			removebar();
			checkremovelogo();

			
		}
	}
	private class RunActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			int index = mytabpane.getSelectedIndex();
			MyTextArea textarea = mytexts.elementAt(index);
			mytabpane.setComponentAt(index, textarea.getRunPanel());
			textarea.RuncloseButton.addActionListener(new CloseRunActionListener());			
		}
	}
	private class ConfigureActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int index = mytabpane.getSelectedIndex();
			MyTextArea textarea = mytexts.elementAt(index);
			mytabpane.setComponentAt(index, textarea.getConfigurePanel());
			textarea.ConfigurecloseButton.addActionListener(new CloseRunActionListener());	
			
		}
	}
	private class CloseRunActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int index = mytabpane.getSelectedIndex();
			MyTextArea textarea = mytexts.elementAt(index);
			Highlighter highlighter = textarea.getTA().getHighlighter();
		    highlighter.removeAllHighlights();
			//runpanel.add(RunContainer, gbc);
			JScrollPane sp = new JScrollPane(textarea.getTA());
			JScrollBar scrollbar=sp.getVerticalScrollBar();
		    scrollbar.setUI(new MyScrollBar());
			sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			mytabpane.setComponentAt(index, sp);
			
		}
	}
	
	
	
	
	//http://www.java2s.com/Code/Java/Swing-JFC/UndoExample2.htm
	  // The Undo action
	public class UndoAction extends AbstractAction {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public UndoAction(UndoManager manager) {
	      this.manager = manager;
	     
	    }
	    public void actionPerformed(ActionEvent evt) {
	      try {
	        manager.undo();

	      } catch (CannotUndoException e) {
	        Toolkit.getDefaultToolkit().beep();

	      }
	    }
	    private UndoManager manager;
	}
	// The Redo action	
	public class RedoAction extends AbstractAction {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public RedoAction(UndoManager manager) {
	      this.manager = manager;
	    }

	    public void actionPerformed(ActionEvent evt) {
	    try {
	        manager.redo();
	      } catch (CannotRedoException e) {
	        Toolkit.getDefaultToolkit().beep();
	      }
	    }

	    private UndoManager manager;
	  }
	
	private void setImages(){
		
		//Image LogoImage;
		try {
			IconImage =ImageIO.read(new File("Resources/img/icon/office.png"));
			
			setIconImage(IconImage);
			Class<?> application = Class.forName("com.apple.eawt.Application");
    		Method getApplication = application.getMethod("getApplication");
    		Method setDockIcon = application.getMethod("setDockIconImage", java.awt.Image.class);
    		Object macOSXApplication = getApplication.invoke(null);
    		setDockIcon.invoke(macOSXApplication,IconImage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JDialog.setDefaultLookAndFeelDecorated(true);   
	}
	public void setInitialPanel(){
		InitialPanel = new MyInitialPanel(this);
		add(InitialPanel);
	}
	
	private void setFont(){
		UIManager.put("TabbedPane.selected",ColorUIResource.orange);
	    UIManager.put("TabbedPane.background",ColorUIResource.black);
	    UIManager.put("TabbedPane.foreground",ColorUIResource.white);
		try {
			//http://stackoverflow.com/questions/5652344/how-can-i-use-a-custom-font-in-java
		    customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Resources/fonts/kenvector_future.ttf")).deriveFont(13f);
			UIManager.put("Menu.font",customFont);
			UIManager.put("MenuItem.font",customFont);
			
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}  
	}
	
	
	
}
