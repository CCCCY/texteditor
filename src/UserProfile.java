import java.io.File;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

public class UserProfile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String encryptedpassword;
	private String command;
	private String filename;
	private String filecontent;
	public String command_loginattempt = "Loginattempt";
	public String command_signupattempt = "signupattempt";
	public String command_loginverify = "loginverify";
	public String command_signupverify = "signupverify";
	public String command_uploadverify = "uploadverify";
	public String command_downloadverify = "downloadverify";
	public boolean loginapproved;
	public boolean signupapproved;
	public boolean uploadsuccess;
	public boolean downloadsuccess;
	public Vector<String> files;
	public UserProfile(String username, String password){
		command_loginattempt = "Loginattempt";
		command_signupattempt = "signupattempt";
		command_loginverify = "loginverify";
		loginapproved = false;
		this.username = username;
		this.encryptedpassword = encryptpassword(password);
		files = new Vector<String>();
	}
	
	public String getName() {
		return username;
	}
	public void setCommand(String incommand){
		command = incommand;
	}
	public String getCommand(){
		return command;
	}
	private String encryptpassword(String password){
		//http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
		try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            encryptedpassword = sb.toString();
            return encryptedpassword;
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
		return "a";
	}
	public String getEncryptedPassword(){
		return encryptedpassword;
	}
	public String getFileName(){
		return filename;
	}
	public String getFileContent(){
		return filecontent;
		
	}
	public void downloadfile(String filename){
		this.filecontent = null;
		this.filename = filename;
	}
	public void uploadfile(String filename, String filecontent){
		
		this.filename = filename;
		this.filecontent = filecontent;
		
		
	}
	public void setFileContent(String filecontent){
		this.filecontent = filecontent;
	}
	public void updatefiles(Vector<String> infile){
		files = infile;
	}
	
	
	

}
