
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MyBackgroundPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private Image BackgroundImage;

	public MyBackgroundPanel(){
		super();
		this.setBackground(Color.gray);
		try {
			BackgroundImage =ImageIO.read(new File("Resources/img/backgrounds/trojan.png"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = (this.getWidth()-BackgroundImage.getWidth(null))/2;
		int height =  (this.getHeight()-BackgroundImage.getWidth(null))/2;
		g.drawImage(BackgroundImage, width, height+80,null);
	}


}
