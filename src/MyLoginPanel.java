import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;


public class MyLoginPanel extends JPanel{


	private static final long serialVersionUID = 1L;
	private Image BackgroundImage;
	private MyButton LoginButton;
	private MainWindowGUI mainwindow;
	private JLabel usernamelabel;
	private JLabel passwordlabel;
	private JTextField usernamefield;
	private JPasswordField passwordfield;
	
	private int port;
	private String hostname;
	public MyLoginPanel(MainWindowGUI windowin, String hostname, int port){
		super();
		setFont();
		this.hostname = hostname;
		this.port = port;
		mainwindow = windowin;
		this.setBackground(Color.gray);
		try {
			BackgroundImage =ImageIO.read(new File("Resources/img/backgrounds/trojan.png"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initializeVariables();
		createGUI();
		
	}
	private void initializeVariables(){
		LoginButton = new MyButton("Login");
		LoginButton.addActionListener(new LoginListener());
		usernamelabel = new JLabel("username: ");
		passwordlabel = new JLabel("password: ");
		usernamefield = new JTextField(18);
		passwordfield = new JPasswordField(18);
		
	}
	private void createGUI(){
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(usernamelabel,gbc);
		gbc.gridx = 1;
		add(usernamefield,gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(passwordlabel,gbc);
		gbc.gridx = 1;
		add(passwordfield,gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		add(LoginButton, gbc);
		
	}
	private void setFont(){
		try {
			//http://stackoverflow.com/questions/5652344/how-can-i-use-a-custom-font-in-java
		    Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Resources/fonts/kenvector_future.ttf")).deriveFont(13f);
			UIManager.put("Button.font",customFont);
			UIManager.put("Label.font",customFont);

		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}  
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = (this.getWidth()-BackgroundImage.getWidth(null))/2;
		int height =  (this.getHeight()-BackgroundImage.getWidth(null))/2+30;
		g.drawImage(BackgroundImage, width, height-5,null);
	}
	private class LoginListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub	
			if(usernamefield.getText().length()==0 ||passwordfield.getText().length()==0){
				return;
			}
			Socket s = null;
			try {
				s = new Socket(hostname, port);
				UserProfile attemptuser = new UserProfile(usernamefield.getText(),passwordfield.getText());
				ClientCommunicator com = new ClientCommunicator(s,mainwindow);
				com.sendloginattempt(attemptuser);	
			} catch (IOException ioe) {
				System.out.println("ioe: " + ioe.getMessage());
				JOptionPane.showMessageDialog(MyLoginPanel.this,
					    "Server cannot be reached.\n Program is in offline mode",
					    "Log-in Failed",
					    JOptionPane.WARNING_MESSAGE);
				System.out.println("ioe: " + ioe.getMessage());
				mainwindow.enterOffline();

			}
		}
	}

}
