import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class MyInitialPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private Image BackgroundImage;
	private MyButton LoginButton;
	private MyButton SignButton;
	private MyButton OffButton;
	private MainWindowGUI mainwindow;
	public MyInitialPanel(MainWindowGUI themainwindow){
		super();
		mainwindow = themainwindow;
		setFont();
		try {
			BackgroundImage =ImageIO.read(new File("Resources/img/backgrounds/trojan.png"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initializeVariables();
		createGUI();
		
	}
	private void initializeVariables(){
		LoginButton = new MyButton("Login");
		LoginButton.addActionListener(new LoginListener());
		SignButton = new MyButton("SignUp");
		SignButton.addActionListener(new SignUpListener());
		OffButton = new MyButton("OffLine");
		OffButton.addActionListener(new OfflineActionListener());
	}
	private void createGUI(){
		this.setBackground(Color.gray);
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(LoginButton, gbc);
		gbc.gridx = 1;
		add(SignButton, gbc);
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.gridy = 1;
		add(OffButton, gbc);
		
	}
	private void setFont(){
		try {
			
			//http://stackoverflow.com/questions/5652344/how-can-i-use-a-custom-font-in-java
		    Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Resources/fonts/kenvector_future.ttf")).deriveFont(13f);
			UIManager.put("Button.font",customFont);
			UIManager.put("Label.font",customFont);

		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}  
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = (this.getWidth()-BackgroundImage.getWidth(null))/2;
		int height =  (this.getHeight()-BackgroundImage.getWidth(null))/2+30;
		g.drawImage(BackgroundImage, width, height-5,null);
	}
	private class OfflineActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			mainwindow.enterOffline();
		}
		
	}
	private class LoginListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			mainwindow.InitialPanel.setVisible(false);
			mainwindow.add(mainwindow.LoginPanel);
		}
	
		
	}
	private class SignUpListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			mainwindow.InitialPanel.setVisible(false);
			mainwindow.add(mainwindow.SignupPanel);
		}
	
		
	}
}
