
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class myMenuBar extends JMenuBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JMenu firstmenu;
	JMenu secondmenu;
	JMenu thirdmenu;
	JMenuItem newitem;
	JMenuItem openitem;
	JMenuItem saveitem;
	JMenuItem closeitem;
	JMenuItem undoitem;
	JMenuItem redoitem;
	JMenuItem cutitem;
	JMenuItem copyitem;
	JMenuItem pasteitem;
	JMenuItem selectitem;
	JMenuItem runitem;
	JMenuItem configureitem;
	

	public myMenuBar(){
		super();
		createComponents();
		createGUI();
		setUI();
	}
	
	
	private void createComponents(){
		//create menus
		firstmenu = new JMenu("File");
		secondmenu = new JMenu("Edit");
		thirdmenu = new JMenu("SpellCheck");
		firstmenu = new JMenu("File");
		firstmenu.setMnemonic('F');
		secondmenu = new JMenu("Edit");
		secondmenu.setMnemonic('E');
		thirdmenu = new JMenu("SpellCheck");
		thirdmenu.setMnemonic('S');
		//create menuitems
		newitem = new JMenuItem("New");
		newitem.setMnemonic('N');
		newitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.CTRL_MASK));
	//	newitem.setToolTipText(text);
		
		openitem = new JMenuItem("Open");
		openitem.setMnemonic('O');
		openitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.CTRL_MASK));
		
		saveitem = new JMenuItem("Save");
		saveitem.setMnemonic('S');
		saveitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK));
		
		closeitem = new JMenuItem("Close");
		closeitem.setMnemonic('C');

		undoitem = new JMenuItem("Undo");
		undoitem.setMnemonic('U');
		undoitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK));
		
		redoitem = new JMenuItem("Redo");
		redoitem.setMnemonic('R');
		redoitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y,ActionEvent.CTRL_MASK));
	
		cutitem = new JMenuItem("Cut");
		cutitem.setMnemonic('C');
		cutitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.CTRL_MASK));
		
		copyitem = new JMenuItem("Copy");
		copyitem.setMnemonic('C');
		copyitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK));
		
		pasteitem = new JMenuItem("Paste");
		pasteitem.setMnemonic('P');
		pasteitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,ActionEvent.CTRL_MASK));
		
		selectitem = new JMenuItem("Select All");
		selectitem.setMnemonic('S');
		selectitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.CTRL_MASK));
		
		runitem = new JMenuItem("Run");
		runitem.setMnemonic('R');
		runitem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7,ActionEvent.CTRL_MASK));
		configureitem = new JMenuItem("Configure");
		configureitem.setMnemonic('C');
		
	}
	
	private void createGUI(){
		firstmenu.add(newitem);
		firstmenu.add(openitem);
		firstmenu.add(saveitem);
		firstmenu.add(closeitem);
		secondmenu.add(undoitem);
		secondmenu.add(redoitem);
		secondmenu.addSeparator();
		secondmenu.add(cutitem);
		secondmenu.add(copyitem);
		secondmenu.add(pasteitem);
		secondmenu.addSeparator();
		secondmenu.add(selectitem);
		thirdmenu.add(runitem);
		thirdmenu.add(configureitem);
				
		
		add(firstmenu);
		add(secondmenu);
		removetwo();
		add(thirdmenu);
		
	}
	private void setUI(){
		ImageIcon newicon = new ImageIcon("Resources/img/menuitems/new.png");
		ImageIcon openicon = new ImageIcon("Resources/img/menuitems/open.png");
		ImageIcon saveicon = new ImageIcon("Resources/img/menuitems/save.png");
		ImageIcon closeicon = new ImageIcon("Resources/img/menuitems/close.png");
		ImageIcon undoicon = new ImageIcon("Resources/img/menuitems/undo.png");
		ImageIcon redoicon = new ImageIcon("Resources/img/menuitems/redo.png");
		ImageIcon cuticon = new ImageIcon("Resources/img/menuitems/cut.png");
		ImageIcon copyicon = new ImageIcon("Resources/img/menuitems/copy.png");
		ImageIcon pasteicon = new ImageIcon("Resources/img/menuitems/paste.png");
		ImageIcon selecticon = new ImageIcon("Resources/img/menuitems/select.png");
		ImageIcon runicon = new ImageIcon("Resources/img/menuitems/run.png");
		ImageIcon configureicon = new ImageIcon("Resources/img/menuitems/configure.png");
		newitem.setIcon(newicon);;
		openitem.setIcon(openicon);;
		saveitem.setIcon(saveicon);;
		closeitem.setIcon(closeicon);;
		undoitem.setIcon(undoicon);;
		redoitem.setIcon(redoicon);;
		cutitem.setIcon(cuticon);;
		copyitem.setIcon(copyicon);;
		pasteitem.setIcon(pasteicon);;
		selectitem.setIcon(selecticon);;
		runitem.setIcon(runicon);;
		configureitem.setIcon(configureicon);;

		
	}
	@Override
	 public void paintComponent(Graphics g) {
		    try {
		        Image img = ImageIO.read(new File("Resources/img/menu/red_button11.png"));             
		        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), null);
		    } catch (IOException e) {
		        e.printStackTrace();
		    }

		
	 };
	
	public void removetwo(){
		secondmenu.setVisible(false);
		thirdmenu.setVisible(false);
	}
	public void addtwo(){
		secondmenu.setVisible(true);
		thirdmenu.setVisible(true);
		
	}
}
