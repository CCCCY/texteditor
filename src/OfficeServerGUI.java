

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
 

public class OfficeServerGUI extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JTextArea textarea;
	private JScrollPane textareapane;
	private MyButton stopButton;
	private int portnumber;
	private OfficeServerListener serverListener;
	private OfficeSQLDriver officesql;
	public OfficeServerGUI(int port){
		super("server");
		officesql = new OfficeSQLDriver();
		portnumber = port;
		setSize(500, 400);
		setLocation(100,50);
		initializeVariables();
		createGUI();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	private void initializeVariables(){
		textarea = new JTextArea();
		textarea.setEditable(false);
		textareapane = new JScrollPane(textarea);
		JScrollBar scrollbar=textareapane.getVerticalScrollBar();
	    scrollbar.setUI(new MyScrollBar());
		textareapane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	    stopButton = new MyButton("Start");
	    stopButton.addActionListener(new StopActionListener());
	}
	
	private class StopActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(stopButton.getText().equals("Start")){
				stopButton.setNewText("Stop");
				listenForConnections();
				textarea.append("Server started on port: "+portnumber+'\n');
				officesql.connect();
			}
			else{
				stopButton.setNewText("Start");
				textarea.append("Server stopped."+'\n');	
				if(serverListener!=null){
					serverListener.stopconnection();
				}
				officesql.stop();
			}
		}
		
	}

	private void listenForConnections() {
		serverListener = new OfficeServerListener(portnumber,this,officesql);
		serverListener.start();
	}
	private void createGUI(){
		this.add(textareapane, BorderLayout.CENTER);
		this.add(stopButton,BorderLayout.SOUTH);
		
	}
	public void setLoginAttempt(UserProfile user){
		textarea.append("Log-in attempt User: "+user.getName()+" Pass: "+user.getEncryptedPassword()+'\n');

	}
	public void setLoginVerify(String username, boolean success,String password){
		if(success){
			textarea.append("Log-in success User: "+username+'\n');
		}else{
			textarea.append("Log-in failure User: "+username+'\n');

		}

	}
	public void setDownloadAttempt(UserProfile user){
		if(user.downloadsuccess){
			textarea.append("File download User: "+user.getName()+" File:"+user.getFileName()+'\n');
		}

		
	}
	public void setUploadAttempt(UserProfile user){
		if(user.uploadsuccess){
			textarea.append("File saved User: "+user.getName()+" File:"+user.getFileName()+'\n');
		}
		
	}
	public void setSignupVerify(String username, boolean success){
		if(success){
			textarea.append("Sign-up success User: "+username+'\n');
		}else{
			textarea.append("Sign-up failure User: "+username+'\n');

		}

	}
	public void setSignupAttempt(UserProfile signupuser){
		textarea.append("Sign-up attempt User: "+signupuser.getName()+" Pass: "+signupuser.getEncryptedPassword()+'\n');
	}
	
}
