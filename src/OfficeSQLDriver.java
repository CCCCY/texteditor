import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;


public class OfficeSQLDriver {
	private Connection con;
	private final static String selectName = "SELECT * FROM User WHERE username = ?";
	private final static String addUser = "INSERT INTO User(username,hashedpassword) VALUES(?,?)";
	private final static String uploadFile = "INSERT INTO Files(username,filename,filecontent) VALUES(?,?,?)";
	private final static String getFilename = "SELECT * FROM Files WHERE username = ?";
	private final static String downloadFile = "SELECT * FROM Files WHERE username = ? and filename = ?";
	private final static String replaceFile = "UPDATE Files SET filecontent = ? WHERE username = ? and filename = ?";
	private final static String existFile =  "SELECT * FROM Files WHERE username = ? and filename = ?";
	public OfficeSQLDriver(){
		try {
			new com.mysql.jdbc.Driver();
			connect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	public void connect(){
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost/chen475Office?user=root&password=root&useSSL=false");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean exist(String username){
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean verifylogin(UserProfile newuser){
		String username = newuser.getName();
		String hashedpassword = newuser.getEncryptedPassword();
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				if(result.getString(3).equals(hashedpassword)){
					updateFiles(newuser);
					return true;
				}
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void addUserProfile(UserProfile newuser){
			try {
				PreparedStatement ps = con.prepareStatement(addUser);
				ps.setString(1, newuser.getName());
				ps.setString(2, newuser.getEncryptedPassword());
				ps.executeUpdate();
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	public boolean uploadFile(UserProfile newuser){
		try {
			if(con.isClosed())return false;
			if(existFile(newuser)){
				replaceFile(newuser);
			}
			else{
				PreparedStatement ps = con.prepareStatement(uploadFile);
				ps.setString(1, newuser.getName());
				ps.setString(2, newuser.getFileName());
				ps.setString(3, newuser.getFileContent());
				ps.executeUpdate();
			}
			updateFiles(newuser);
			return true;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return false;

		
	}
	private boolean existFile(UserProfile uploaduser){
		try {
			PreparedStatement ps = con.prepareStatement(existFile);
			ps.setString(1, uploaduser.getName());
			ps.setString(2, uploaduser.getFileName());
			ResultSet result = ps.executeQuery();
			if(result.next()){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	private void replaceFile(UserProfile uploaduser){
		try {
			PreparedStatement ps = con.prepareStatement(replaceFile);	
			ps.setString(1, uploaduser.getFileContent());
			ps.setString(2, uploaduser.getName());
			ps.setString(3, uploaduser.getFileName());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean downloadFile(UserProfile downloaduser){

		try{
			if(con.isClosed())return false;
			PreparedStatement ps = con.prepareStatement(downloadFile);
			ps.setString(1, downloaduser.getName());
			ps.setString(2, downloaduser.getFileName());
			ResultSet result = ps.executeQuery();
			if(result.next()){
				downloaduser.setFileContent(result.getString(3));
				return true;
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;

		}
		return false;

	}
	
	public void updateFiles(UserProfile user){
		Vector<String> filenames = new Vector<String>();
		try{
			PreparedStatement ps = con.prepareStatement(getFilename);
			ps.setString(1, user.getName());
			ResultSet result = ps.executeQuery();
			while(result.next()){
				filenames.add(result.getString(2));
				
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		user.updatefiles(filenames);
	}
	
	public void stop(){
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
