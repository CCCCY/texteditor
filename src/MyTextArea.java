
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

public class MyTextArea {
	JTextArea ta;
	JScrollPane sp;
	JScrollPane runsp;
	JScrollPane configuresp;
	MyButton RuncloseButton;
	MyButton ConfigurecloseButton;
	MyButton IgnoreButton;
	MyButton AddButton;
	MyButton ChangeButton;
	MyButton KeyboardButton;
	MyButton WordlistButton;
	JLabel WordLabel;
	JLabel KeyboardLabel;
	JLabel WordlistLabel;
	JComboBox<String> WordBox;
	Vector<String> suggestwords;
	File file;
	JPanel runpanel;
	JPanel configurepanel;
	Scanner wordlist;
	Scanner keyboard;
	Scanner text;
	WordManager wordmanager;
	Vector<Vector<String>> resultwords;
	File wordlistfile;
	File keyboardfile;
	String currword;
	int start;
	int end;
	HashMap<String,Integer> countmap;
	Font customFont;
	public MyTextArea(){
		setFont();

		ta = new JTextArea();
		ta.setSelectionColor(Color.orange);
		ta.setLineWrap(true);
    	ta.setWrapStyleWord(true);
		sp = new JScrollPane(ta);
	    JScrollBar scrollbar=sp.getVerticalScrollBar();
	    scrollbar.setUI(new MyScrollBar());
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		try{
			if(wordlistfile==null)	wordlistfile = new File("wordlist.wl.txt");
			if(keyboardfile==null)	keyboardfile = new File("qwerty-us.kb.txt");
			wordlist = new Scanner(wordlistfile);
			keyboard = new Scanner(keyboardfile);
			
		}
		catch(FileNotFoundException e){
				System.out.println("configure files not found");
		}
	}
	
	
	public MyTextArea(File newfile){
		setFont();
		ta = new JTextArea();
		ta.setSelectionColor(Color.orange);
		ta.setLineWrap(true);
    	ta.setWrapStyleWord(true);
		sp = new JScrollPane(ta);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		file = newfile;
	//http://stackoverflow.com/questions/7033589/how-to-import-a-text-file-content-to-a-jtextarea-in-a-java-application	
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(file));
			// TODO Auto-generated catch block		
			String line = in.readLine();
			while(line != null){
				ta.append(line + "\n");
				line = in.readLine();
			}
		} catch( Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			if(wordlistfile==null)	wordlistfile = new File("wordlist.wl.txt");
			if(keyboardfile==null)	keyboardfile = new File("qwerty-us.kb.txt");
			wordlist = new Scanner(wordlistfile);
			keyboard = new Scanner(keyboardfile);
			
		}
		catch(FileNotFoundException e){
				System.out.println("configure files not found");
		}
	}
	public void setText(String text){
		ta.setText(text);
	}
	
	public void setFile(File newfile){
		file = newfile;
	}
	
	public JPanel getRunPanel(){
		try {
			wordlist = new Scanner(wordlistfile);
			keyboard = new Scanner(keyboardfile);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if((wordlist == null) || (keyboard == null)) return new JPanel();
		countmap = new HashMap<String,Integer>();
		wordmanager = new WordManager(wordlist, keyboard);
		text = new Scanner(ta.getText());
		runpanel = new JPanel();
		runpanel.setBackground(Color.gray);
		runpanel.setVisible(true);
		runpanel.setLayout(new GridBagLayout());
		
		//add scrollpane
		runsp = new JScrollPane(ta);
		JScrollBar scrollbar=runsp.getVerticalScrollBar();
	    scrollbar.setUI(new MyScrollBar());
		runsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.9;
		gbc.weighty = 1;		
		runpanel.add(runsp,gbc);
		
		//add runcontainer
		JPanel RunContainer = new JPanel();
		RunContainer.setBackground(Color.gray);
        TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Spell Check");
        titledBorder.setTitleFont(customFont);
		RunContainer.setBorder(titledBorder);
		RunContainer.setLayout(new BorderLayout());
		JPanel ButtonContainer = new JPanel();
		JPanel CenterPanel = new JPanel();
		ButtonContainer.setBackground(Color.gray);
		CenterPanel.setBackground(Color.gray);
		CenterPanel.setLayout(new GridLayout(3,1));
		ButtonContainer.setLayout(new GridBagLayout());
		
		WordLabel = new JLabel();
		WordLabel.setText("  Spelling: ");
		IgnoreButton = new MyButton("Ignore");
		IgnoreButton.addActionListener(new IgnoreListener());
		RuncloseButton = new MyButton("Close");
		RuncloseButton.setText("close");
		AddButton = new MyButton("Add");
		AddButton.addActionListener(new AddListener());
		ChangeButton = new MyButton("Change");
		ChangeButton.addActionListener(new ChangeListener());
		suggestwords = new Vector<String>();
		WordBox = new JComboBox<String>(suggestwords);
		WordBox.setUI(new MyComboBoxUI());

		GridBagConstraints gbc2 = new GridBagConstraints();
		gbc2.gridx = 0;
		gbc2.gridy = 0;
		gbc2.ipadx = 15;

		ButtonContainer.add(IgnoreButton, gbc2);
		gbc2.gridx = 1;
		gbc2.ipadx = 42;
		ButtonContainer.add(AddButton, gbc2);
		gbc2.gridx = 0;
		gbc2.gridy = 1;

		ButtonContainer.add(WordBox, gbc2);
		gbc2.gridx = 1;
		gbc2.ipadx = 20;

		ButtonContainer.add(ChangeButton, gbc2);
		CenterPanel.add(ButtonContainer);
		//create GUI
		RunContainer.add(WordLabel, BorderLayout.NORTH);
		RunContainer.add(CenterPanel, BorderLayout.CENTER);
		RunContainer.add(RuncloseButton, BorderLayout.SOUTH);
		

		gbc.weightx = 0.1;
		runpanel.add(RunContainer, gbc);
		RunText();
		text.close();
		wordlist.close();
		keyboard.close();
		return runpanel;
		
	}
	public JPanel getConfigurePanel(){
		configurepanel = new JPanel();
		configurepanel.setVisible(true);
		configurepanel.setLayout(new GridBagLayout());
		
		//add scrollpane
		configuresp = new JScrollPane(ta);
		JScrollBar scrollbar=configuresp.getVerticalScrollBar();
	    scrollbar.setUI(new MyScrollBar());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 0.9;
		gbc.weighty = 1;		
		configurepanel.add(configuresp,gbc);
		
		//add configurecontainer
		JPanel ConfigureContainer = new JPanel();
		ConfigureContainer.setBackground(Color.gray);
		TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "Configure");
        titledBorder.setTitleFont(customFont);
		ConfigureContainer.setBorder(titledBorder);
        ConfigureContainer.setLayout(new BorderLayout());
		JPanel TopContainer = new JPanel();
		TopContainer.setBackground(Color.gray);
		TopContainer.setLayout(new GridLayout(4,1));
		KeyboardLabel = new JLabel(keyboardfile.getName());
		WordlistLabel = new JLabel(wordlistfile.getName());
		KeyboardButton = new MyButton(keyboardfile.getName());
		KeyboardButton.addActionListener(new KeyboardListener());
		WordlistButton = new MyButton(wordlistfile.getName());
		WordlistButton.addActionListener(new WordlistListener());
		ConfigurecloseButton = new MyButton("Close");
		//create GUI
		TopContainer.add(WordlistLabel);
		TopContainer.add(WordlistButton);
		TopContainer.add(KeyboardLabel);
		TopContainer.add(KeyboardButton);
		ConfigureContainer.add(TopContainer, BorderLayout.NORTH);
		ConfigureContainer.add(ConfigurecloseButton, BorderLayout.SOUTH);
		

		gbc.weightx = 0.1;
		configurepanel.add(ConfigureContainer, gbc);

		return configurepanel;
		
	}
	private void RunText(){
		Highlighter highlighter = ta.getHighlighter();
	    highlighter.removeAllHighlights();
		resultwords = new Vector<Vector<String> >();
		text = new Scanner(ta.getText());
		while(text.hasNext()){
			String temp = text.next();
			if(!wordmanager.isCorrect(temp)){
				Vector<String> result = wordmanager.getList(temp);
				resultwords.add(result);
				
			}
		}
		
		if(resultwords.size()>0){
			setWord(resultwords.get(0));
		}
		
	}
	private void setWord(Vector<String> current){
		String tempstring = current.get(0);
		WordLabel.setText("  Spelling: "+current.get(0));
		currword = current.get(0);
		suggestwords = current;
		suggestwords.remove(0);
		
		WordBox.removeAllItems();
		for(int i=0; i<suggestwords.size();i++){
			WordBox.addItem(suggestwords.elementAt(i));
		}
		Highlighter highlighter = ta.getHighlighter();
	    HighlightPainter painter =  new DefaultHighlighter.DefaultHighlightPainter(Color.orange);
	    highlighter.removeAllHighlights();
	    String temptext = ta.getText().toLowerCase();
	    int count=0;
	    if(countmap.containsKey(tempstring)){
	    	count = countmap.get(tempstring);
	    }
	    start = temptext.indexOf(tempstring,count);
	    end = start + tempstring.length();
	    try {
			highlighter.addHighlight(start, end, painter );
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public File getFile(){
		return file;
	}
	
	public JTextArea getTA(){
		return ta;
	}
	public JScrollPane getSP(){
		return sp;
	}
	private class WordlistListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open File...");
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setMultiSelectionEnabled(false);
	     //   fc.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
	        Component parent = null;
	        int returnVal = fc.showOpenDialog(parent);
	        if(returnVal == JFileChooser.APPROVE_OPTION){
	        	File openedfile = fc.getSelectedFile();
	        	wordlistfile = openedfile;
	        	WordlistLabel.setText(openedfile.getName());
	        	WordlistButton.setText(openedfile.getName());
	        	
	        }

			
		}
		
	}
	private class KeyboardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open File...");
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setMultiSelectionEnabled(false);
	     //   fc.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
	        Component parent = null;
	        int returnVal = fc.showOpenDialog(parent);
	        if(returnVal == JFileChooser.APPROVE_OPTION){
	        	File openedfile = fc.getSelectedFile();
	        	keyboardfile = openedfile;
	        	KeyboardLabel.setText(openedfile.getName());
	        	KeyboardButton.setText(openedfile.getName());
	        }
		}
		
	}
	private class ChangeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String selectedword = (String) WordBox.getSelectedItem();
			ta.replaceRange(selectedword, start, end);	
			if(resultwords.size()==0) return;
			resultwords.remove(0);
			if(resultwords.size()==0) return;
			setWord(resultwords.elementAt(0));
			
		}
		
	}
	private class AddListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				FileWriter fw = new FileWriter(wordlistfile, true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw);
			    out.println(currword);
				out.close();
				Vector<Integer> toremove = new Vector<Integer>();
				for(int i=0; i<resultwords.size();i++){
					Vector<String> temp = resultwords.get(i);
					if(temp.size()>0){
						if(temp.get(0).equals(currword)){
							toremove.add(i);
						}
					}
				}
				
				for(int i = toremove.size()-1; i>=0 ; i--){
					resultwords.removeElementAt(toremove.get(i));
				}
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(resultwords.size()==0)  {removehighlighter();return;}
			resultwords.remove(0);
			if(resultwords.size()==0)  {removehighlighter();return;}
			setWord(resultwords.elementAt(0));
			
		}
		
	}
	private void setFont(){
		try {
			
			//http://stackoverflow.com/questions/5652344/how-can-i-use-a-custom-font-in-java
		    customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Resources/fonts/kenvector_future.ttf")).deriveFont(13f);
			UIManager.put("Button.font",customFont);
			UIManager.put("Label.font",customFont);

		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			System.out.println("asd");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("asd");

		}  
	}
	
	private void removehighlighter(){
		Highlighter highlighter = ta.getHighlighter();
	    highlighter.removeAllHighlights();
	    WordBox.removeAllItems();
	    WordLabel.setText(" Spelling:");

	}
	private class IgnoreListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			countmap.put(currword, start+1);
			if(resultwords.size()==0)  {removehighlighter();return;}
			resultwords.remove(0);
			if(resultwords.size()==0)  {removehighlighter();return;}
			setWord(resultwords.elementAt(0));
			
		}
		
	}
}
