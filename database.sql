DROP DATABASE IF EXISTS chen475Office;
CREATE DATABASE chen475Office;
USE chen475Office;
CREATE TABLE User(
	UserID int(11) primary key auto_increment,
    username varchar(18) not null,
    hashedpassword varchar(100) not null
);

CREATE TABLE Files(
	username varchar(18) not null,
    filename varchar(50) not null,
    filecontent varchar(3000) not null
);
